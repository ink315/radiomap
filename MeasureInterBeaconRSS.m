%Inter Beacon RSS Measurement

function [InterMap, MeasureInterMap]=MeasureInterBeaconRSS(blockeffect, block)
global M;
global T;
global RssBeaconCoordinates;
InterMap=zeros(M,M,T);
MeasureInterMap=zeros(M,M);

    for i=1:1:M
        for j=1:1:M
            dis=sqrt((RssBeaconCoordinates(i,1)-RssBeaconCoordinates(j,1))^2+(RssBeaconCoordinates(i,2)-RssBeaconCoordinates(j,2))^2);
            for t=1:1:T
                InterMap(i,j,t)=PropModel(dis);
                if(crossblock(block, RssBeaconCoordinates(i,:), RssBeaconCoordinates(j,:)) == 1)
                    InterMap(i,j,t)=InterMap(i,j,t)-blockeffect;
                end
            end
            MeasureInterMap(i,j)=mean(InterMap(i,j,:));
        end
    end