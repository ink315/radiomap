% calculate beta

function Qe=CalInterBeaconRSS(InterMap, Beta)

global M;
global T;

Qt=zeros(M,M,T);
Qe=zeros(M,M);

    for i=1:1:M
        for j=1:1:M
            for t=1:1:T
                refindex=[1:1:M];
                refpower=[InterMap(i,refindex) 1];  %%%important change
                sumtemp=0;
                for mm=1:1:M+1
                    sumtemp=sumtemp+Beta(i,j,mm)*refpower(mm);
                end
                Qt(i,j,t)=sumtemp;
            end
            Qe(i,j)=mean(Qt(i,j,:));
        end
    end