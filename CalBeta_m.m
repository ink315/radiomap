% calculate beta

function Beta=CalBeta_m(InterMap)         %%%%%%需要去掉q_ij

    global M;
    global T;
    Beta=zeros(M,M,M);
   
    for i=1:1:M
        for j=1:1:M
            if(i==j)
                Beta(i,j,:)=0;
            else
                for t=1:1:T
                    Y_ij(t)=InterMap(i,j,t);
                    set1=[1:1:M];
                    set2=[i,j];
                    uindex=setdiff(set1,set2);
                    for u=1:1:M-2              %%%%获得矩阵Q，注意Beacon自己对自己无检测信号强度
                        Q_mut(t,u)=InterMap(i,uindex(u),t);
                    end
                end
                temppara=inv(Q_mut'*Q_mut)*Q_mut'*Y_ij';       
                for u=1:1:M
                    if(u<min(i,j))
                        Beta(i,j,u)=temppara(u);             
                    elseif(u==min(i,j))
                        Beta(i,j,u)=0;
                    elseif(u>min(i,j)&&u<max(i,j))
                        Beta(i,j,u)=temppara(u-1);
                    elseif(u==max(i,j))
                        Beta(i,j,u)=0;
                    elseif(u>max(i,j))
                        Beta(i,j,u)=temppara(u-2); 
                    end
                end
            end
        end

    end
    