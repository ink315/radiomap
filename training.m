close all, clear all, clc;

global M; %number of RSS beacons
global N; %number of grids;
global R; %communication range of TOA beacons
global Delta; %variance of TOA measurement

global pt; %transmission power of RSS beacons;
global p0; %reference path loss at 1m;
global pvar; % random noise in path loss

global Area; %sensing field;
global RssBeaconCoordinates; %coordinates of RSS beacons
global TOABeaconCoordinates; %coordinates of TOA beacons
global GridSize;
global Map;
global Map_beacon;
global T; %training time (long enough)

M=10; %number of beacons
GridSize=1;
Area=[20, 20];
pt=150;
p0=1;
pvar=2;
R=25;
Delta = 1;
T=30; 

W=Area(1)/GridSize;
L=Area(2)/GridSize; % number of training locations;

RssBeaconCoordinates=zeros(M,2); %location of RSS beacons;
% generate Location of Rss Beacons randomly
%
for i=1:1:M
    RssBeaconCoordinates(i,1)=rand*Area(1);
    RssBeaconCoordinates(i,2)=rand*Area(2);
end
%

hold on;
plot(RssBeaconCoordinates(:,1), RssBeaconCoordinates(:,2),'ro','linewidth',2);
axis([0,Area(1),0,Area(2)]);

Map_beacon = zeros(M, M ,T); % record RSS between beacons
Map = zeros(W, L, M, T);     % record RSS between beacons and locations
Dis_From_Beacons=zeros(W,L,M);


%RSS measurements between beacons
Dis=zeros(M,M);
for i=1:M
    for j=1:M
        dis=sqrt((RssBeaconCoordinates(i,1)-RssBeaconCoordinates(j,1))^2+(RssBeaconCoordinates(i,2)-RssBeaconCoordinates(j,2))^2);
        for t=1:1:T
            Map_beacon(i,j,t)=PropModel(dis); % normrnd in model simulates RSS time variance
        end
        %}
    end
end
%RSS measurements between beacons and locations
%
for i=1:1:W
    for j=1:1:L
        for m=1:1:M
            dis=sqrt(((i-0.5)*GridSize-RssBeaconCoordinates(m,1))^2+((j-0.5)*GridSize-RssBeaconCoordinates(m,2))^2);
            Dis_From_Beacons(i,j,m)=dis;
            for t=1:1:T
                Map(i,j,m,t)=PropModel(dis);
            end
        end
    end
end

%Model=zeros(W,L,M+1);
Alpha=zeros(W,L,M,M+1);
for i=1:1:W
    for j=1:1:L
        for m=1:1:M
            for t=1:1:T
                Y_ijm(t)=Map(i,j,m,t);
                for u=1:1:M-1              %%%%获得矩阵Q，注意Beacon自己对自己无检测信号强度
                    if(u<m)
                        Q_mut(t,u)=Map_beacon(m,u,t);   
                    else
                        Q_mut(t,u)=Map_beacon(m,u+1,t);
                    end
                end
                Q_mut(t,M)=1;
            end
            temppara=inv(Q_mut'*Q_mut)*Q_mut'*Y_ijm';        %%%计算系数矩阵 Alpha_ij=inv(A_ij' * A_ij)*A_ij' Q_ij;
            for u=1:1:M+1
                if(u<m)
                     Alpha(i,j,m,u)=temppara(u);             %%%这一步的作用是使得Alpha_ij(m,m)=0;
                elseif(u==m)
                    Alpha(i,j,m,u)=0;
                else
                    Alpha(i,j,m,u)=temppara(u-1);
                end
            end
            %Alpha(i,j,m,:)=inv(Q_mut'*Q_mut)*Q_mut'*Y_ijm';
        end

    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The calculated radio map

for i=1:1:W
    for j=1:1:L
        for m=1:1:M
            for t=1:1:T
                refindex=[1:1:M];
                refpower=[Map_beacon(m,refindex) 1];
                sumtemp=0;
                for mm=1:1:M+1
                    sumtemp=sumtemp+Alpha(i,j,m,mm)*refpower(mm);
                end
                CalMap(i,j,m,t)=sumtemp;
            end
        end
    end
end

%%%%%%%%%%%%%%%%
% Radio Map Consistency Checking
%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% find the representive node
   Belongs=zeros(W,L);
   for i=1:1:W
       for j=1:1:L
           shortest=10000;
           closestbeacon=M+1;
           for m=1:1:M
               if(Dis_From_Beacons(i,j,m)<shortest)
                   shortest=Dis_From_Beacons(i,j,m);
                   closestbeacon=m;
               end
           end
           Belongs(j,i)=closestbeacon;
       end
   end
  % figure
  % surf(Belongs);            
BeaconLocation=zeros(M,2)
for m=1:1:M
    shortest=10000;
    BeaconLocation(m,:)=[0,0];
    for i=1:1:W
        for j=1:1:L
            if(Dis_From_Beacons(i,j,m)<shortest)
                shortest=Dis_From_Beacons(i,j,m);
                BeaconLocation(m,:)=[i,j];
            end
        end
    end
end
  figure
  surf(Belongs);         

figure;
surf(1:1:W, 1:1:L, Map(:,:,2,1));
title('measured radio map');

figure;
surf(1:1:W, 1:1:L, CalMap(:,:,2,1));
title('calculated radio map using the trained coeficients');

     

save beacon_setup.mat