% calculate the distances between calInterMap and MeasureInterMap

function disMatrix=MapDis(MeasureInterMap, CalInterMap)

global M;
disMatrix=zeros(M,M);

for i=1:1:M
    for j=1:1:M
        disMatrix(i,j)=MeasureInterMap(i,j)-CalInterMap(i,j);
    end
end

