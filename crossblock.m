% check whether the link cross the block


function y=crossblock(block, X1, X2)

%global RssBeaconCoordinates; %coordinates of RSS beacons
%global TOABeaconCoordinates; %coordinates of TOA beacons

y=0;
if((X1(2)<=block(2)) && (X2(2)>block(2)))
    y=1;
elseif(X1(2)>=block(2) && X2(2)<block(2))
    y=1;
else
    ;
end