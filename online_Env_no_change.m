% Hblock
clear;
close all;

global pt;

load beacon_setup.mat

block=[0,0, Area(1),0];
blockeffect=20;

InterMap=zeros(M,M,T);
Beta=zeros(M,M,M+1);
mapdistance=zeros(M,M);
Threshold=1;


for time=1:1:20
    
    x=Area(1)/T*time;
    target(time,1)=x;
    target(time,2)=Area(2)/2*(sin(x*pi*2/Area(1))+1);
    
    %%%%%%%%目标在线获得多个beacon的信号强度
    for m=1:1:M 
        dis=sqrt((target(time,1)-RssBeaconCoordinates(m,1))^2+(target(time,2)-RssBeaconCoordinates(m,2))^2);
        S(m,time)=PropModel(dis);
        if(crossblock(block, target(time,:), RssBeaconCoordinates(m,:)) == 1)
             S(m, time)=S(m, time)-blockeffect;
        end
    end
    
  
    %%%%%%%%Beacon之间的信号地图的在线测量
    [InterMap, MeasureInterMap]=MeasureInterBeaconRSS(blockeffect, block);
    
%     figure;
%     surf(MeasureInterMap(:,:));
%     title('Measured intermap');
 
    %%%%%%%% Beta，Beacon之间的拟合参数，在初始化的时候进行计算
    if(time == 1)
        Beta=CalBeta(InterMap);
    end
    
    %%%%%%% 根据实测的InterMap，当前的Beta计算出一个“计算版本”的Intermap
    %CalInterMap=CalInterBeaconRSS(InterMap, Beta);
    

    %%%%%%% 评价“计算版本”InterMap 同 "测量版本" Intermap之间的距离
    %mapdistance=MapDis(MeasureInterMap, CalInterMap);
    %MEANDIS = mean(mean(abs(mapdistance))) ;
    
    %%%%%% 如果“计算版本”InterMap 同 "测量版本" Intermap之间的距离大于设定预支，则重新计算Beta
    %if(MEANDIS>Threshold)
    Beta=CalBeta(InterMap);
    
        
%     CalInterMap=CalInterBeaconRSS(InterMap(:,:,1), Beta);
%     
%     for i=1:1:M
%         for j=1:1:M
%             temp=0;
%             refindex=[1:1:M];
%             refpower=[InterMap(i,refindex, 2) 1];
%             for k=1:1:M+1
%                 temp=temp+Beta(i,j,k)*refpower(k);
%             end
%             CalInterMap(i,j)=temp;
%         end
%     end
%     
%     
%     figure;
%     surf(CalInterMap(:,:));
%     hold on;
%     surf(MeasureInterMap(:,:));
%     title('calculated intermap by Beta');
% %     
        
    %%%%%% Beta被更新之后，同时更新Alpha 
        Lamda=zeros(W,L,M);
        
        for w=1:1:W
            for l=1:1:L
                for i=1:1:M
                    for k=1:1:M                      
                        temp=0;
                        for j=1:1:M
                            temp=temp+Alpha(w,l,i,j)*Beta(i,j,k);
                        end
                        Alpha1(w,l,i,k)=temp;     %更新非常数项系数
                    end
                    temp=0;
                    for j=1:1:M
                          temp=temp+Alpha(w,l,i,j)*Beta(i,j,M+1);
                    end
                                          
                    Alpha1(w,l,i,M+1)=temp+Alpha(w,l,i,M+1);    %%%%%更新常数项系数
                end
                
            end
        end
        
        %%%%  更新常数项的稀疏Lamda
        %%%% 算法思路找到距离(w,l)最近的beacon，设为c, 令其测量其它beacon的信号强度，
        %%%% 根据几次测得的 Ri,c'=alpha1*q_{i,k}'+lamda_{c,i} * Alpha(w,l,i,M+1)
        %%%%% 计算lamda_c，对于位于beacon c 的voronoi 格子范围内的位置，其信号强度的线性模型，
        %%%%% 共享同一个常数项变换系数。
         
        
        
        
    Alpha=Alpha1;
    
%%%%%%%%%%%%%%%%%%%%%%绘制实测的radio map

    for i=1:1:W
        for j=1:1:L
            for m=1:1:M
                dis=sqrt(((i-0.5)*GridSize-RssBeaconCoordinates(m,1))^2+((j-0.5)*GridSize-RssBeaconCoordinates(m,2))^2);
                Dis_to_Beacons(i,j,m)=dis;
                MeasureMap(i,j,m)=PropModel(dis);
                cordi=[i,j];
                if(crossblock(block, cordi, RssBeaconCoordinates(m,:)) == 1)
                   MeasureMap(i,j,m)=MeasureMap(i,j,m)-blockeffect;
                end

            end
        end
    end
    
   
    figure;
    surf(MeasureMap(:,:,9));
    title('measured radio map');

%%%%%%%%%%%%%%%%%%%%%%绘制当前参数下计算的的radio map
    
    for w=1:1:W
        for l=1:1:L
            for i=1:1:M
                refindex=[1:1:M];
                refpower=[MeasureInterMap(i,refindex), 1];
                sumtemp=0;
                for k=1:1:M+1
                    sumtemp=sumtemp+Alpha(w,l,i,k)*refpower(k);
                end
                NewCalMap(w,l,i)=sumtemp;
            end
        end
    end
    
%     figure;
%     surf(NewCalMap(:,:,5));
%     title('calculated radio map by new alpha');
%        
 
%%%获得各个beacon计算的RSS
    
    for i=1:1:M
        for m=1:1:M
            %dis=sqrt(((BeaconLocation(i,1)-0.5)*GridSize-RssBeaconCoordinates(m,1))^2+((BeaconLocation(i,2)-0.5)*GridSize-RssBeaconCoordinates(m,2))^2);
            CalBeaconRSS(i,m,time)=NewCalMap(BeaconLocation(i,1),BeaconLocation(i,2),m);

        end
    end
    
%     figure
%     surf(CalBeaconRSS);
%     hold on;
%     surf(MeasureInterMap);
    
   

 
            
    
    %%%%%%%%根据在线测量的Beacon InterMap，和当前的Alpha向量，计算在每个Grid上M个Beacon的信号强度
    for i=1:1:W
        for j=1:1:L
            for m=1:1:M
                refindex=[1:1:M];
                refpower=[MeasureInterMap(m,refindex) 1];
                sumtemp=0;
                for mm=1:1:M+1
                    sumtemp=sumtemp+Alpha(i,j,m,mm)*refpower(mm);
                end
                CalRSS(i,j,m)=sumtemp;
                
            end
        end
    end
    
    %%%%%%%%根据Beacon的在线信号强度的测量值，调整每个位置的信号强度至。
    
    for nb=1:1:M
        for m=1:1:M
            [I,J]=find(Belongs==nb);
            if(m~=nb)
                for k=1:1:size(I,1)
                    CalRSS(J(k),I(k),m)=CalRSS(J(k),I(k),m)*MeasureInterMap(m,nb)/CalBeaconRSS(m,nb,time);                
                end
            else
                if(time==1)
                    ;
                else
                    for k=1:1:size(I,1)
                        CalRSS(J(k),I(k),m)=CalRSS(J(k),I(k),m)*CalBeaconRSS(m,nb,time)/CalBeaconRSS(m,nb,1);                
                    end
                end
                
            end
            
        end
    end    
    
    figure;
    surf(CalRSS(:,:,9));
    title('calculated radio map by new alpha');
           
    
    %%%%% 根据Target的实测信号强度，和推算出来的每个Grid上的信号强度，查找信号强度最为匹配的位置，输出为定位结果
    mindis=10000;
    locate=[100,100];
    for i=1:1:W
        for j=1:1:L
            temp=[];
            for m=1:1:M
                temp=[temp; CalRSS(i,j,m)];
            end
            RSSDIS = sqrt(sum((temp-S(:,time)).^2));
            if(RSSDIS < mindis)
                mindis=RSSDIS;
                locate=[i,j];
            end
        end
    end
    track(time,1)=locate(1);
    track(time,2)=locate(2);
    
    
    %%%%%% 空间中的障碍物，随着时间缓慢移动其位置，产生对信号强度的严重影响。
    %block=block+[0, 1, 0, 1];
    pt=pt-2;

end
    
%

%%%%%%绘制目标实际轨迹和跟踪结果
figure;
plot(target(:,1),target(:,2),'r*-',track(:,1),track(:,2),'bs-');    
    
    

