% Hblock
clear;
close all;

global pt;

load beacon_setup.mat

block=[0,0, Area(1),0];
blockeffect=0;

InterMap=zeros(M,M,T);
Beta=zeros(M,M,M+1);
mapdistance=zeros(M,M);
Threshold=1;

for time=1:1:10
    
    x=Area(1)/T*time;
    target(time,1)=x;
    target(time,2)=Area(2)/2*(sin(x*pi*2/Area(1))+1);
    
    %%%%%%%%目标在线获得多个beacon的信号强度
    for m=1:1:M 
        dis=sqrt((target(time,1)-RssBeaconCoordinates(m,1))^2+(target(time,2)-RssBeaconCoordinates(m,2))^2);
        S(m,time)=PropModel(dis);
        if(crossblock(block, target(time,:), RssBeaconCoordinates(m,:)) == 1)
             S(m, time)=S(m, time)-blockeffect;
        end
    end
    
  
    %%%%%%%%Beacon之间的信号地图的在线测量
    [InterMap, MeasureInterMap]=MeasureInterBeaconRSS(blockeffect, block);
    
%     figure;
%     surf(MeasureInterMap(:,:));
%     title('Measured intermap');
 
    %%%%%%%% Beta，Beacon之间的拟合参数，在初始化的时候进行计算
    if(time == 1)
        Beta=CalBeta(InterMap);
    end
    
    %%%%%%% 根据实测的InterMap，当前的Beta计算出一个“计算版本”的Intermap
    %CalInterMap=CalInterBeaconRSS(InterMap, Beta);
    

    %%%%%%% 评价“计算版本”InterMap 同 "测量版本" Intermap之间的距离
    %mapdistance=MapDis(MeasureInterMap, CalInterMap);
    %MEANDIS = mean(mean(abs(mapdistance))) ;
    
    %%%%%% 如果“计算版本”InterMap 同 "测量版本" Intermap之间的距离大于设定预支，则重新计算Beta
    %if(MEANDIS>Threshold)
    Beta=CalBeta(InterMap);
    
    i=1;
        for j=1:1:M
            for k=1:1:M+1
            Test(j,k)=Beta(i,j,k);
            end
        end
        for k=1:1:M
            Test(M+1,k)=0;
        end
         Test(M+1,M+1)=1;
%         eig(Test)
        
    %CalInterMap=CalInterBeaconRSS(InterMap, Beta);
    
%     for i=1:1:M
%         for j=1:1:M
%             temp=0;
%             refindex=[1:1:M];
%             refpower=[MeasureInterMap(i,refindex) 1];
%             for k=1:1:M+1
%                 temp=temp+Beta(i,j,k)*refpower(k);
%             end
%             CalInterMap(i,j)=temp;
%         end
%     end
%     
%     
%     figure;
%     surf(CalInterMap(:,:));
%     title('calculated intermap by Beta');
%     
        
    %%%%%% Beta被更新之后，同时更新Alpha
    %for round=1:1:200
        
        for w=1:1:W
            for l=1:1:L
                for i=1:1:M
                    for k=1:1:M                      
                        temp=0;
                        for j=1:1:M
                            temp=temp+Alpha(w,l,i,j)*Beta(i,j,k);
                        end
                        Alpha1(w,l,i,k)=temp;
                    end
                    temp=0;
                    for j=1:1:M
                          temp=temp+Alpha(w,l,i,j)*Beta(i,j,M+1);
                    end
                    Alpha1(w,l,i,M+1)=temp+Alpha(w,l,i,M+1);    
                end
            end
        end

    %end


    %end
    
%%%%%%%%%%%%%%%%%%%%%%绘制当前参数下计算的radio map
    for w=1:1:W
        for l=1:1:L
            for i=1:1:M
                refindex=[1:1:M];
                refpower=[MeasureInterMap(i,refindex), 0.1];
                sumtemp=0;
                for k=1:1:M+1
                    sumtemp=sumtemp+Alpha(w,l,i,k)*refpower(k);
                end
%                 sumtemp1=0;
%                 for k=1:1:M
%                     sumtemp2=0;
%                     for j=1:1:M
%                         sumtemp2=sumtemp2+Beta(i,k,j)*refpower(j);
%                     end
%                     sumtemp2=sumtemp2+Beta(i,k,M+1);
%                     sumtemp1=sumtemp1+Alpha(w,l,i,k)*sumtemp2;
%                 end

                NewCalMap(w,l,i)=sumtemp;
%                 NewCalMap1(w,l,i)=sumtemp1+Alpha(w,l,i,M+1);

            end
        end
    end

    figure;
    surf(NewCalMap(:,:,4));
    title('measured radio map by Beta');
% %     
    for i=1:1:W
        for j=1:1:L
            for m=1:1:M
                dis=sqrt(((i-0.5)*GridSize-RssBeaconCoordinates(m,1))^2+((j-0.5)*GridSize-RssBeaconCoordinates(m,2))^2);
                Dis_to_Beacons(i,j,m)=dis;
                MeasureMap(i,j,m)=PropModel(dis);
                cordi=[i,j];
                if(crossblock(block, cordi, RssBeaconCoordinates(m,:)) == 1)
                   MeasureMap(i,j,m)=MeasureMap(i,j,m)-blockeffect;
                end

            end
        end
    end
    
    figure;
    surf(MeasureMap(:,:,4));
    title('measured radio map');
    
    %%%%%%%%根据在线测量的Beacon InterMap，和当前的Alpha向量，计算在每个Grid上M个Beacon的信号强度
    for i=1:1:W
        for j=1:1:L
            for m=1:1:M
                refindex=[1:1:M];
                refpower=[MeasureInterMap(m,refindex) 0.1];
                sumtemp=0;
                for mm=1:1:M+1
                    sumtemp=sumtemp+Alpha(i,j,m,mm)*refpower(mm);
                end
                CalRSS(i,j,m)=sumtemp;
            end
        end
    end
    
    %%%%% 根据Target的实测信号强度，和推算出来的每个Grid上的信号强度，查找信号强度最为匹配的位置，输出为定位结果
    mindis=10000;
    locate=[100,100];
    for i=1:1:W
        for j=1:1:L
            temp=[];
            for m=1:1:M
                temp=[temp; CalRSS(i,j,m)];
            end
            RSSDIS = sqrt(sum((temp-S(:,time)).^2));
            if(RSSDIS < mindis)
                mindis=RSSDIS;
                locate=[i,j];
            end
        end
    end
    track(time,1)=locate(1);
    track(time,2)=locate(2);
    
    
    %%%%%% 空间中的障碍物，随着时间缓慢移动其位置，产生对信号强度的严重影响。
    %block=block+[0, 4, 0, 4];
    pt=pt-1;

end
    
%

%%%%%%绘制目标实际轨迹和跟踪结果
figure;
plot(target(:,1),target(:,2),'r*-',track(:,1),track(:,2),'bs-');    
    
    

