function flag = Compare_accuracy(X_begin,X_update,X_accurate)

%X_accurate: accurate coefficients, calculated in each time slot
%X_begin: coefficients at time t=1
%X_update: coefficients if updated

%flag: to show whether the adaptive idea is better

a = (X_begin - X_accurate).^2;
b = (X_update - X_accurate).^2;
c = sqrt(sum(sum(a)));
d = sqrt(sum(sum(b)));
if c > d
    flag = 1;% update is adaptive, better
else
    flag = 0;
end